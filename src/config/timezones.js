export const getStaticTimezones = () => {
	return [
    {
        "id": 1,
        "name": "Kabul, Afghanistan",
        "gmt": "+04:30"
    },
    {
        "id": 2,
        "name": "Mariehamn, Aland Islands",
        "gmt": "+02:00"
    },
    {
        "id": 3,
        "name": "Tirane, Albania",
        "gmt": "+01:00"
    },
    {
        "id": 4,
        "name": "Algiers, Algeria",
        "gmt": "+01:00"
    },
    {
        "id": 5,
        "name": "Pago Pago, American Samoa",
        "gmt": "-11:00"
    },
    {
        "id": 6,
        "name": "Andorra, Andorra",
        "gmt": "+01:00"
    },
    {
        "id": 7,
        "name": "Luanda, Angola",
        "gmt": "+01:00"
    },
    {
        "id": 8,
        "name": "Anguilla, Anguilla",
        "gmt": "-04:00"
    },
    {
        "id": 9,
        "name": "Casey, Antarctica",
        "gmt": "+11:00"
    },
    {
        "id": 10,
        "name": "Davis, Antarctica",
        "gmt": "+07:00"
    },
    {
        "id": 11,
        "name": "DumontDUrville, Antarctica",
        "gmt": "+10:00"
    },
    {
        "id": 12,
        "name": "Mawson, Antarctica",
        "gmt": "+05:00"
    },
    {
        "id": 13,
        "name": "McMurdo, Antarctica",
        "gmt": "+13:00"
    },
    {
        "id": 14,
        "name": "Palmer, Antarctica",
        "gmt": "-03:00"
    },
    {
        "id": 15,
        "name": "Rothera, Antarctica",
        "gmt": "-03:00"
    },
    {
        "id": 16,
        "name": "Syowa, Antarctica",
        "gmt": "+03:00"
    },
    {
        "id": 17,
        "name": "Troll, Antarctica",
        "gmt": "00:00"
    },
    {
        "id": 18,
        "name": "Vostok, Antarctica",
        "gmt": "+06:00"
    },
    {
        "id": 19,
        "name": "Antigua, Antigua and Barbuda",
        "gmt": "-04:00"
    },
    {
        "id": 20,
        "name": "Buenos Aires, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 21,
        "name": "Catamarca, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 22,
        "name": "Cordoba, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 23,
        "name": "Jujuy, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 24,
        "name": "La Rioja, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 25,
        "name": "Mendoza, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 26,
        "name": "Rio Gallegos, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 27,
        "name": "Salta, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 28,
        "name": "San Juan, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 29,
        "name": "San Luis, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 30,
        "name": "Tucuman, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 31,
        "name": "Ushuaia, Argentina",
        "gmt": "-03:00"
    },
    {
        "id": 32,
        "name": "Yerevan, Armenia",
        "gmt": "+04:00"
    },
    {
        "id": 33,
        "name": "Aruba, Aruba",
        "gmt": "-04:00"
    },
    {
        "id": 34,
        "name": "Macquarie, Australia",
        "gmt": "+11:00"
    },
    {
        "id": 35,
        "name": "Adelaide, Australia",
        "gmt": "+10:30"
    },
    {
        "id": 36,
        "name": "Brisbane, Australia",
        "gmt": "+10:00"
    },
    {
        "id": 37,
        "name": "Broken Hill, Australia",
        "gmt": "+10:30"
    },
    {
        "id": 38,
        "name": "Currie, Australia",
        "gmt": "+11:00"
    },
    {
        "id": 39,
        "name": "Darwin, Australia",
        "gmt": "+09:30"
    },
    {
        "id": 40,
        "name": "Eucla, Australia",
        "gmt": "+08:45"
    },
    {
        "id": 41,
        "name": "Hobart, Australia",
        "gmt": "+11:00"
    },
    {
        "id": 42,
        "name": "Lindeman, Australia",
        "gmt": "+10:00"
    },
    {
        "id": 43,
        "name": "Lord Howe, Australia",
        "gmt": "+11:00"
    },
    {
        "id": 44,
        "name": "Melbourne, Australia",
        "gmt": "+11:00"
    },
    {
        "id": 45,
        "name": "Perth, Australia",
        "gmt": "+08:00"
    },
    {
        "id": 46,
        "name": "Sydney, Australia",
        "gmt": "+11:00"
    },
    {
        "id": 47,
        "name": "Vienna, Austria",
        "gmt": "+01:00"
    },
    {
        "id": 48,
        "name": "Baku, Azerbaijan",
        "gmt": "+04:00"
    },
    {
        "id": 49,
        "name": "Nassau, Bahamas",
        "gmt": "-05:00"
    },
    {
        "id": 50,
        "name": "Bahrain, Bahrain",
        "gmt": "+04:00"
    },
    {
        "id": 51,
        "name": "Dhaka, Bangladesh",
        "gmt": "+06:00"
    },
    {
        "id": 52,
        "name": "Barbados, Barbados",
        "gmt": "-04:00"
    },
    {
        "id": 53,
        "name": "Minsk, Belarus",
        "gmt": "+03:00"
    },
    {
        "id": 54,
        "name": "Brussels, Belgium",
        "gmt": "+01:00"
    },
    {
        "id": 55,
        "name": "Belize, Belize",
        "gmt": "-06:00"
    },
    {
        "id": 56,
        "name": "Porto-Novo, Benin",
        "gmt": "+01:00"
    },
    {
        "id": 57,
        "name": "Bermuda, Bermuda",
        "gmt": "-04:00"
    },
    {
        "id": 58,
        "name": "Thimphu, Bhutan",
        "gmt": "+06:00"
    },
    {
        "id": 59,
        "name": "La Paz, Bolivia",
        "gmt": "-04:00"
    },
    {
        "id": 60,
        "name": "Kralendijk, Bonaire, Saint Eustatius and Saba",
        "gmt": "-04:00"
    },
    {
        "id": 61,
        "name": "Sarajevo, Bosnia and Herzegovina",
        "gmt": "+01:00"
    },
    {
        "id": 62,
        "name": "Gaborone, Botswana",
        "gmt": "+02:00"
    },
    {
        "id": 63,
        "name": "Araguaina, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 64,
        "name": "Bahia, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 65,
        "name": "Belem, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 66,
        "name": "Boa Vista, Brazil",
        "gmt": "-04:00"
    },
    {
        "id": 67,
        "name": "Campo Grande, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 68,
        "name": "Cuiaba, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 69,
        "name": "Eirunepe, Brazil",
        "gmt": "-05:00"
    },
    {
        "id": 70,
        "name": "Fortaleza, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 71,
        "name": "Maceio, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 72,
        "name": "Manaus, Brazil",
        "gmt": "-04:00"
    },
    {
        "id": 73,
        "name": "Noronha, Brazil",
        "gmt": "-02:00"
    },
    {
        "id": 74,
        "name": "Porto Velho, Brazil",
        "gmt": "-04:00"
    },
    {
        "id": 75,
        "name": "Recife, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 76,
        "name": "Rio Branco, Brazil",
        "gmt": "-05:00"
    },
    {
        "id": 77,
        "name": "Santarem, Brazil",
        "gmt": "-03:00"
    },
    {
        "id": 78,
        "name": "Sao Paulo, Brazil",
        "gmt": "-02:00"
    },
    {
        "id": 79,
        "name": "Chagos, British Indian Ocean Territory",
        "gmt": "+06:00"
    },
    {
        "id": 80,
        "name": "Tortola, British Virgin Islands",
        "gmt": "-04:00"
    },
    {
        "id": 81,
        "name": "Brunei, Brunei",
        "gmt": "+08:00"
    },
    {
        "id": 82,
        "name": "Sofia, Bulgaria",
        "gmt": "+02:00"
    },
    {
        "id": 83,
        "name": "Ouagadougou, Burkina Faso",
        "gmt": "00:00"
    },
    {
        "id": 84,
        "name": "Bujumbura, Burundi",
        "gmt": "+02:00"
    },
    {
        "id": 85,
        "name": "Phnom Penh, Cambodia",
        "gmt": "+07:00"
    },
    {
        "id": 86,
        "name": "Douala, Cameroon",
        "gmt": "+01:00"
    },
    {
        "id": 87,
        "name": "Atikokan, Canada",
        "gmt": "-05:00"
    },
    {
        "id": 88,
        "name": "Blanc-Sablon, Canada",
        "gmt": "-04:00"
    },
    {
        "id": 89,
        "name": "Cambridge Bay, Canada",
        "gmt": "-07:00"
    },
    {
        "id": 90,
        "name": "Creston, Canada",
        "gmt": "-07:00"
    },
    {
        "id": 91,
        "name": "Dawson, Canada",
        "gmt": "-08:00"
    },
    {
        "id": 92,
        "name": "Dawson Creek, Canada",
        "gmt": "-07:00"
    },
    {
        "id": 93,
        "name": "Edmonton, Canada",
        "gmt": "-07:00"
    },
    {
        "id": 94,
        "name": "Fort Nelson, Canada",
        "gmt": "-07:00"
    },
    {
        "id": 95,
        "name": "Glace Bay, Canada",
        "gmt": "-04:00"
    },
    {
        "id": 96,
        "name": "Goose Bay, Canada",
        "gmt": "-04:00"
    },
    {
        "id": 97,
        "name": "Halifax, Canada",
        "gmt": "-04:00"
    },
    {
        "id": 98,
        "name": "Inuvik, Canada",
        "gmt": "-07:00"
    },
    {
        "id": 99,
        "name": "Iqaluit, Canada",
        "gmt": "-05:00"
    },
    {
        "id": 100,
        "name": "Moncton, Canada",
        "gmt": "-04:00"
    },
    {
        "id": 101,
        "name": "Nipigon, Canada",
        "gmt": "-05:00"
    },
    {
        "id": 102,
        "name": "Pangnirtung, Canada",
        "gmt": "-05:00"
    },
    {
        "id": 103,
        "name": "Rainy River, Canada",
        "gmt": "-06:00"
    },
    {
        "id": 104,
        "name": "Rankin Inlet, Canada",
        "gmt": "-06:00"
    },
    {
        "id": 105,
        "name": "Regina, Canada",
        "gmt": "-06:00"
    },
    {
        "id": 106,
        "name": "Resolute, Canada",
        "gmt": "-06:00"
    },
    {
        "id": 107,
        "name": "St Johns, Canada",
        "gmt": "-03:30"
    },
    {
        "id": 108,
        "name": "Swift Current, Canada",
        "gmt": "-06:00"
    },
    {
        "id": 109,
        "name": "Thunder Bay, Canada",
        "gmt": "-05:00"
    },
    {
        "id": 110,
        "name": "Toronto, Canada",
        "gmt": "-05:00"
    },
    {
        "id": 111,
        "name": "Vancouver, Canada",
        "gmt": "-08:00"
    },
    {
        "id": 112,
        "name": "Whitehorse, Canada",
        "gmt": "-08:00"
    },
    {
        "id": 113,
        "name": "Winnipeg, Canada",
        "gmt": "-06:00"
    },
    {
        "id": 114,
        "name": "Yellowknife, Canada",
        "gmt": "-07:00"
    },
    {
        "id": 115,
        "name": "Cape Verde, Cape Verde",
        "gmt": "-01:00"
    },
    {
        "id": 116,
        "name": "Cayman, Cayman Islands",
        "gmt": "-05:00"
    },
    {
        "id": 117,
        "name": "Bangui, Central African Republic",
        "gmt": "+01:00"
    },
    {
        "id": 118,
        "name": "Ndjamena, Chad",
        "gmt": "+01:00"
    },
    {
        "id": 119,
        "name": "Punta Arenas, Chile",
        "gmt": "-03:00"
    },
    {
        "id": 120,
        "name": "Santiago, Chile",
        "gmt": "-03:00"
    },
    {
        "id": 121,
        "name": "Easter, Chile",
        "gmt": "-05:00"
    },
    {
        "id": 122,
        "name": "Shanghai, China",
        "gmt": "+08:00"
    },
    {
        "id": 123,
        "name": "Urumqi, China",
        "gmt": "+06:00"
    },
    {
        "id": 124,
        "name": "Christmas, Christmas Island",
        "gmt": "+07:00"
    },
    {
        "id": 125,
        "name": "Cocos, Cocos Islands",
        "gmt": "+06:30"
    },
    {
        "id": 126,
        "name": "Bogota, Colombia",
        "gmt": "-05:00"
    },
    {
        "id": 127,
        "name": "Comoro, Comoros",
        "gmt": "+03:00"
    },
    {
        "id": 128,
        "name": "Rarotonga, Cook Islands",
        "gmt": "-10:00"
    },
    {
        "id": 129,
        "name": "Costa Rica, Costa Rica",
        "gmt": "-06:00"
    },
    {
        "id": 130,
        "name": "Zagreb, Croatia",
        "gmt": "+01:00"
    },
    {
        "id": 131,
        "name": "Havana, Cuba",
        "gmt": "-05:00"
    },
    {
        "id": 132,
        "name": "Curacao, Curaçao",
        "gmt": "-04:00"
    },
    {
        "id": 133,
        "name": "Famagusta, Cyprus",
        "gmt": "+02:00"
    },
    {
        "id": 134,
        "name": "Nicosia, Cyprus",
        "gmt": "+02:00"
    },
    {
        "id": 135,
        "name": "Prague, Czech Republic",
        "gmt": "+01:00"
    },
    {
        "id": 136,
        "name": "Kinshasa, Democratic Republic of the Congo",
        "gmt": "+01:00"
    },
    {
        "id": 137,
        "name": "Lubumbashi, Democratic Republic of the Congo",
        "gmt": "+02:00"
    },
    {
        "id": 138,
        "name": "Copenhagen, Denmark",
        "gmt": "+01:00"
    },
    {
        "id": 139,
        "name": "Djibouti, Djibouti",
        "gmt": "+03:00"
    },
    {
        "id": 140,
        "name": "Dominica, Dominica",
        "gmt": "-04:00"
    },
    {
        "id": 141,
        "name": "Santo Domingo, Dominican Republic",
        "gmt": "-04:00"
    },
    {
        "id": 142,
        "name": "Dili, East Timor",
        "gmt": "+09:00"
    },
    {
        "id": 143,
        "name": "Guayaquil, Ecuador",
        "gmt": "-05:00"
    },
    {
        "id": 144,
        "name": "Galapagos, Ecuador",
        "gmt": "-06:00"
    },
    {
        "id": 145,
        "name": "Cairo, Egypt",
        "gmt": "+02:00"
    },
    {
        "id": 146,
        "name": "El Salvador, El Salvador",
        "gmt": "-06:00"
    },
    {
        "id": 147,
        "name": "Malabo, Equatorial Guinea",
        "gmt": "+01:00"
    },
    {
        "id": 148,
        "name": "Asmara, Eritrea",
        "gmt": "+03:00"
    },
    {
        "id": 149,
        "name": "Tallinn, Estonia",
        "gmt": "+02:00"
    },
    {
        "id": 150,
        "name": "Addis Ababa, Ethiopia",
        "gmt": "+03:00"
    },
    {
        "id": 151,
        "name": "Stanley, Falkland Islands",
        "gmt": "-03:00"
    },
    {
        "id": 152,
        "name": "Faroe, Faroe Islands",
        "gmt": "00:00"
    },
    {
        "id": 153,
        "name": "Fiji, Fiji",
        "gmt": "+13:00"
    },
    {
        "id": 154,
        "name": "Helsinki, Finland",
        "gmt": "+02:00"
    },
    {
        "id": 155,
        "name": "Paris, France",
        "gmt": "+01:00"
    },
    {
        "id": 156,
        "name": "Cayenne, French Guiana",
        "gmt": "-03:00"
    },
    {
        "id": 157,
        "name": "Gambier, French Polynesia",
        "gmt": "-09:00"
    },
    {
        "id": 158,
        "name": "Marquesas, French Polynesia",
        "gmt": "-09:30"
    },
    {
        "id": 159,
        "name": "Tahiti, French Polynesia",
        "gmt": "-10:00"
    },
    {
        "id": 160,
        "name": "Kerguelen, French Southern Territories",
        "gmt": "+05:00"
    },
    {
        "id": 161,
        "name": "Libreville, Gabon",
        "gmt": "+01:00"
    },
    {
        "id": 162,
        "name": "Banjul, Gambia",
        "gmt": "00:00"
    },
    {
        "id": 163,
        "name": "Tbilisi, Georgia",
        "gmt": "+04:00"
    },
    {
        "id": 164,
        "name": "Berlin, Germany",
        "gmt": "+01:00"
    },
    {
        "id": 165,
        "name": "Busingen, Germany",
        "gmt": "+01:00"
    },
    {
        "id": 166,
        "name": "Accra, Ghana",
        "gmt": "00:00"
    },
    {
        "id": 167,
        "name": "Gibraltar, Gibraltar",
        "gmt": "+01:00"
    },
    {
        "id": 168,
        "name": "Athens, Greece",
        "gmt": "+02:00"
    },
    {
        "id": 169,
        "name": "Danmarkshavn, Greenland",
        "gmt": "00:00"
    },
    {
        "id": 170,
        "name": "Godthab, Greenland",
        "gmt": "-03:00"
    },
    {
        "id": 171,
        "name": "Scoresbysund, Greenland",
        "gmt": "-01:00"
    },
    {
        "id": 172,
        "name": "Thule, Greenland",
        "gmt": "-04:00"
    },
    {
        "id": 173,
        "name": "Grenada, Grenada",
        "gmt": "-04:00"
    },
    {
        "id": 174,
        "name": "Guadeloupe, Guadeloupe",
        "gmt": "-04:00"
    },
    {
        "id": 175,
        "name": "Guam, Guam",
        "gmt": "+10:00"
    },
    {
        "id": 176,
        "name": "Guatemala, Guatemala",
        "gmt": "-06:00"
    },
    {
        "id": 177,
        "name": "Guernsey, Guernsey",
        "gmt": "00:00"
    },
    {
        "id": 178,
        "name": "Conakry, Guinea",
        "gmt": "00:00"
    },
    {
        "id": 179,
        "name": "Bissau, Guinea-Bissau",
        "gmt": "00:00"
    },
    {
        "id": 180,
        "name": "Guyana, Guyana",
        "gmt": "-04:00"
    },
    {
        "id": 181,
        "name": "Port-au-Prince, Haiti",
        "gmt": "-05:00"
    },
    {
        "id": 182,
        "name": "Tegucigalpa, Honduras",
        "gmt": "-06:00"
    },
    {
        "id": 183,
        "name": "Hong Kong, Hong Kong",
        "gmt": "+08:00"
    },
    {
        "id": 184,
        "name": "Budapest, Hungary",
        "gmt": "+01:00"
    },
    {
        "id": 185,
        "name": "Reykjavik, Iceland",
        "gmt": "00:00"
    },
    {
        "id": 186,
        "name": "Kolkata, India",
        "gmt": "+05:30"
    },
    {
        "id": 187,
        "name": "Jakarta, Indonesia",
        "gmt": "+07:00"
    },
    {
        "id": 188,
        "name": "Jayapura, Indonesia",
        "gmt": "+09:00"
    },
    {
        "id": 189,
        "name": "Makassar, Indonesia",
        "gmt": "+08:00"
    },
    {
        "id": 190,
        "name": "Pontianak, Indonesia",
        "gmt": "+07:00"
    },
    {
        "id": 191,
        "name": "Tehran, Iran",
        "gmt": "+03:30"
    },
    {
        "id": 192,
        "name": "Baghdad, Iraq",
        "gmt": "+03:00"
    },
    {
        "id": 193,
        "name": "Dublin, Ireland",
        "gmt": "00:00"
    },
    {
        "id": 194,
        "name": "Isle of Man, Isle of Man",
        "gmt": "00:00"
    },
    {
        "id": 195,
        "name": "Jerusalem, Israel",
        "gmt": "+02:00"
    },
    {
        "id": 196,
        "name": "Rome, Italy",
        "gmt": "+01:00"
    },
    {
        "id": 197,
        "name": "Abidjan, Ivory Coast",
        "gmt": "00:00"
    },
    {
        "id": 198,
        "name": "Jamaica, Jamaica",
        "gmt": "-05:00"
    },
    {
        "id": 199,
        "name": "Tokyo, Japan",
        "gmt": "+09:00"
    },
    {
        "id": 200,
        "name": "Jersey, Jersey",
        "gmt": "00:00"
    },
    {
        "id": 201,
        "name": "Amman, Jordan",
        "gmt": "+02:00"
    },
    {
        "id": 202,
        "name": "Almaty, Kazakhstan",
        "gmt": "+06:00"
    },
    {
        "id": 203,
        "name": "Aqtau, Kazakhstan",
        "gmt": "+05:00"
    },
    {
        "id": 204,
        "name": "Aqtobe, Kazakhstan",
        "gmt": "+05:00"
    },
    {
        "id": 205,
        "name": "Atyrau, Kazakhstan",
        "gmt": "+05:00"
    },
    {
        "id": 206,
        "name": "Oral, Kazakhstan",
        "gmt": "+05:00"
    },
    {
        "id": 207,
        "name": "Qyzylorda, Kazakhstan",
        "gmt": "+06:00"
    },
    {
        "id": 208,
        "name": "Nairobi, Kenya",
        "gmt": "+03:00"
    },
    {
        "id": 209,
        "name": "Enderbury, Kiribati",
        "gmt": "+13:00"
    },
    {
        "id": 210,
        "name": "Kiritimati, Kiribati",
        "gmt": "+14:00"
    },
    {
        "id": 211,
        "name": "Tarawa, Kiribati",
        "gmt": "+12:00"
    },
    {
        "id": 212,
        "name": "Kuwait, Kuwait",
        "gmt": "+03:00"
    },
    {
        "id": 213,
        "name": "Bishkek, Kyrgyzstan",
        "gmt": "+06:00"
    },
    {
        "id": 214,
        "name": "Vientiane, Laos",
        "gmt": "+07:00"
    },
    {
        "id": 215,
        "name": "Riga, Latvia",
        "gmt": "+02:00"
    },
    {
        "id": 216,
        "name": "Beirut, Lebanon",
        "gmt": "+02:00"
    },
    {
        "id": 217,
        "name": "Maseru, Lesotho",
        "gmt": "+02:00"
    },
    {
        "id": 218,
        "name": "Monrovia, Liberia",
        "gmt": "00:00"
    },
    {
        "id": 219,
        "name": "Tripoli, Libya",
        "gmt": "+02:00"
    },
    {
        "id": 220,
        "name": "Vaduz, Liechtenstein",
        "gmt": "+01:00"
    },
    {
        "id": 221,
        "name": "Vilnius, Lithuania",
        "gmt": "+02:00"
    },
    {
        "id": 222,
        "name": "Luxembourg, Luxembourg",
        "gmt": "+01:00"
    },
    {
        "id": 223,
        "name": "Macau, Macao",
        "gmt": "+08:00"
    },
    {
        "id": 224,
        "name": "Skopje, Macedonia",
        "gmt": "+01:00"
    },
    {
        "id": 225,
        "name": "Antananarivo, Madagascar",
        "gmt": "+03:00"
    },
    {
        "id": 226,
        "name": "Blantyre, Malawi",
        "gmt": "+02:00"
    },
    {
        "id": 227,
        "name": "Kuala Lumpur, Malaysia",
        "gmt": "+08:00"
    },
    {
        "id": 228,
        "name": "Kuching, Malaysia",
        "gmt": "+08:00"
    },
    {
        "id": 229,
        "name": "Maldives, Maldives",
        "gmt": "+05:00"
    },
    {
        "id": 230,
        "name": "Bamako, Mali",
        "gmt": "00:00"
    },
    {
        "id": 231,
        "name": "Malta, Malta",
        "gmt": "+01:00"
    },
    {
        "id": 232,
        "name": "Kwajalein, Marshall Islands",
        "gmt": "+12:00"
    },
    {
        "id": 233,
        "name": "Majuro, Marshall Islands",
        "gmt": "+12:00"
    },
    {
        "id": 234,
        "name": "Martinique, Martinique",
        "gmt": "-04:00"
    },
    {
        "id": 235,
        "name": "Nouakchott, Mauritania",
        "gmt": "00:00"
    },
    {
        "id": 236,
        "name": "Mauritius, Mauritius",
        "gmt": "+04:00"
    },
    {
        "id": 237,
        "name": "Mayotte, Mayotte",
        "gmt": "+03:00"
    },
    {
        "id": 238,
        "name": "Bahia Banderas, Mexico",
        "gmt": "-06:00"
    },
    {
        "id": 239,
        "name": "Cancun, Mexico",
        "gmt": "-05:00"
    },
    {
        "id": 240,
        "name": "Chihuahua, Mexico",
        "gmt": "-07:00"
    },
    {
        "id": 241,
        "name": "Hermosillo, Mexico",
        "gmt": "-07:00"
    },
    {
        "id": 242,
        "name": "Matamoros, Mexico",
        "gmt": "-06:00"
    },
    {
        "id": 243,
        "name": "Mazatlan, Mexico",
        "gmt": "-07:00"
    },
    {
        "id": 244,
        "name": "Merida, Mexico",
        "gmt": "-06:00"
    },
    {
        "id": 245,
        "name": "Mexico City, Mexico",
        "gmt": "-06:00"
    },
    {
        "id": 246,
        "name": "Monterrey, Mexico",
        "gmt": "-06:00"
    },
    {
        "id": 247,
        "name": "Ojinaga, Mexico",
        "gmt": "-07:00"
    },
    {
        "id": 248,
        "name": "Tijuana, Mexico",
        "gmt": "-08:00"
    },
    {
        "id": 249,
        "name": "Chuuk, Micronesia",
        "gmt": "+10:00"
    },
    {
        "id": 250,
        "name": "Kosrae, Micronesia",
        "gmt": "+11:00"
    },
    {
        "id": 251,
        "name": "Pohnpei, Micronesia",
        "gmt": "+11:00"
    },
    {
        "id": 252,
        "name": "Chisinau, Moldova",
        "gmt": "+02:00"
    },
    {
        "id": 253,
        "name": "Monaco, Monaco",
        "gmt": "+01:00"
    },
    {
        "id": 254,
        "name": "Choibalsan, Mongolia",
        "gmt": "+08:00"
    },
    {
        "id": 255,
        "name": "Hovd, Mongolia",
        "gmt": "+07:00"
    },
    {
        "id": 256,
        "name": "Ulaanbaatar, Mongolia",
        "gmt": "+08:00"
    },
    {
        "id": 257,
        "name": "Podgorica, Montenegro",
        "gmt": "+01:00"
    },
    {
        "id": 258,
        "name": "Montserrat, Montserrat",
        "gmt": "-04:00"
    },
    {
        "id": 259,
        "name": "Casablanca, Morocco",
        "gmt": "00:00"
    },
    {
        "id": 260,
        "name": "Maputo, Mozambique",
        "gmt": "+02:00"
    },
    {
        "id": 261,
        "name": "Yangon, Myanmar",
        "gmt": "+06:30"
    },
    {
        "id": 262,
        "name": "Windhoek, Namibia",
        "gmt": "+02:00"
    },
    {
        "id": 263,
        "name": "Nauru, Nauru",
        "gmt": "+12:00"
    },
    {
        "id": 264,
        "name": "Kathmandu, Nepal",
        "gmt": "+05:45"
    },
    {
        "id": 265,
        "name": "Amsterdam, Netherlands",
        "gmt": "+01:00"
    },
    {
        "id": 266,
        "name": "Noumea, New Caledonia",
        "gmt": "+11:00"
    },
    {
        "id": 267,
        "name": "Auckland, New Zealand",
        "gmt": "+13:00"
    },
    {
        "id": 268,
        "name": "Chatham, New Zealand",
        "gmt": "+13:45"
    },
    {
        "id": 269,
        "name": "Managua, Nicaragua",
        "gmt": "-06:00"
    },
    {
        "id": 270,
        "name": "Niamey, Niger",
        "gmt": "+01:00"
    },
    {
        "id": 271,
        "name": "Lagos, Nigeria",
        "gmt": "+01:00"
    },
    {
        "id": 272,
        "name": "Niue, Niue",
        "gmt": "-11:00"
    },
    {
        "id": 273,
        "name": "Norfolk, Norfolk Island",
        "gmt": "+11:00"
    },
    {
        "id": 274,
        "name": "Pyongyang, North Korea",
        "gmt": "+08:30"
    },
    {
        "id": 275,
        "name": "Saipan, Northern Mariana Islands",
        "gmt": "+10:00"
    },
    {
        "id": 276,
        "name": "Oslo, Norway",
        "gmt": "+01:00"
    },
    {
        "id": 277,
        "name": "Muscat, Oman",
        "gmt": "+04:00"
    },
    {
        "id": 278,
        "name": "Karachi, Pakistan",
        "gmt": "+05:00"
    },
    {
        "id": 279,
        "name": "Palau, Palau",
        "gmt": "+09:00"
    },
    {
        "id": 280,
        "name": "Gaza, Palestinian Territory",
        "gmt": "+02:00"
    },
    {
        "id": 281,
        "name": "Hebron, Palestinian Territory",
        "gmt": "+02:00"
    },
    {
        "id": 282,
        "name": "Panama, Panama",
        "gmt": "-05:00"
    },
    {
        "id": 283,
        "name": "Bougainville, Papua New Guinea",
        "gmt": "+11:00"
    },
    {
        "id": 284,
        "name": "Port Moresby, Papua New Guinea",
        "gmt": "+10:00"
    },
    {
        "id": 285,
        "name": "Asuncion, Paraguay",
        "gmt": "-03:00"
    },
    {
        "id": 286,
        "name": "Lima, Peru",
        "gmt": "-05:00"
    },
    {
        "id": 287,
        "name": "Manila, Philippines",
        "gmt": "+08:00"
    },
    {
        "id": 288,
        "name": "Pitcairn, Pitcairn",
        "gmt": "-08:00"
    },
    {
        "id": 289,
        "name": "Warsaw, Poland",
        "gmt": "+01:00"
    },
    {
        "id": 290,
        "name": "Azores, Portugal",
        "gmt": "-01:00"
    },
    {
        "id": 291,
        "name": "Madeira, Portugal",
        "gmt": "00:00"
    },
    {
        "id": 292,
        "name": "Lisbon, Portugal",
        "gmt": "00:00"
    },
    {
        "id": 293,
        "name": "Puerto Rico, Puerto Rico",
        "gmt": "-04:00"
    },
    {
        "id": 294,
        "name": "Qatar, Qatar",
        "gmt": "+04:00"
    },
    {
        "id": 295,
        "name": "Brazzaville, Republic of the Congo",
        "gmt": "+01:00"
    },
    {
        "id": 296,
        "name": "Reunion, Reunion",
        "gmt": "+04:00"
    },
    {
        "id": 297,
        "name": "Bucharest, Romania",
        "gmt": "+02:00"
    },
    {
        "id": 298,
        "name": "Anadyr, Russia",
        "gmt": "+12:00"
    },
    {
        "id": 299,
        "name": "Barnaul, Russia",
        "gmt": "+07:00"
    },
    {
        "id": 300,
        "name": "Chita, Russia",
        "gmt": "+09:00"
    },
    {
        "id": 301,
        "name": "Irkutsk, Russia",
        "gmt": "+08:00"
    },
    {
        "id": 302,
        "name": "Kamchatka, Russia",
        "gmt": "+12:00"
    },
    {
        "id": 303,
        "name": "Khandyga, Russia",
        "gmt": "+09:00"
    },
    {
        "id": 304,
        "name": "Krasnoyarsk, Russia",
        "gmt": "+07:00"
    },
    {
        "id": 305,
        "name": "Magadan, Russia",
        "gmt": "+11:00"
    },
    {
        "id": 306,
        "name": "Novokuznetsk, Russia",
        "gmt": "+07:00"
    },
    {
        "id": 307,
        "name": "Novosibirsk, Russia",
        "gmt": "+07:00"
    },
    {
        "id": 308,
        "name": "Omsk, Russia",
        "gmt": "+06:00"
    },
    {
        "id": 309,
        "name": "Sakhalin, Russia",
        "gmt": "+11:00"
    },
    {
        "id": 310,
        "name": "Srednekolymsk, Russia",
        "gmt": "+11:00"
    },
    {
        "id": 311,
        "name": "Tomsk, Russia",
        "gmt": "+07:00"
    },
    {
        "id": 312,
        "name": "Ust-Nera, Russia",
        "gmt": "+10:00"
    },
    {
        "id": 313,
        "name": "Vladivostok, Russia",
        "gmt": "+10:00"
    },
    {
        "id": 314,
        "name": "Yakutsk, Russia",
        "gmt": "+09:00"
    },
    {
        "id": 315,
        "name": "Yekaterinburg, Russia",
        "gmt": "+05:00"
    },
    {
        "id": 316,
        "name": "Astrakhan, Russia",
        "gmt": "+04:00"
    },
    {
        "id": 317,
        "name": "Kaliningrad, Russia",
        "gmt": "+02:00"
    },
    {
        "id": 318,
        "name": "Kirov, Russia",
        "gmt": "+03:00"
    },
    {
        "id": 319,
        "name": "Moscow, Russia",
        "gmt": "+03:00"
    },
    {
        "id": 320,
        "name": "Samara, Russia",
        "gmt": "+04:00"
    },
    {
        "id": 321,
        "name": "Saratov, Russia",
        "gmt": "+04:00"
    },
    {
        "id": 322,
        "name": "Simferopol, Russia",
        "gmt": "+03:00"
    },
    {
        "id": 323,
        "name": "Ulyanovsk, Russia",
        "gmt": "+04:00"
    },
    {
        "id": 324,
        "name": "Volgograd, Russia",
        "gmt": "+03:00"
    },
    {
        "id": 325,
        "name": "Kigali, Rwanda",
        "gmt": "+02:00"
    },
    {
        "id": 326,
        "name": "St Barthelemy, Saint Barthélemy",
        "gmt": "-04:00"
    },
    {
        "id": 327,
        "name": "St Helena, Saint Helena",
        "gmt": "00:00"
    },
    {
        "id": 328,
        "name": "St Kitts, Saint Kitts and Nevis",
        "gmt": "-04:00"
    },
    {
        "id": 329,
        "name": "St Lucia, Saint Lucia",
        "gmt": "-04:00"
    },
    {
        "id": 330,
        "name": "Marigot, Saint Martin",
        "gmt": "-04:00"
    },
    {
        "id": 331,
        "name": "Miquelon, Saint Pierre and Miquelon",
        "gmt": "-03:00"
    },
    {
        "id": 332,
        "name": "St Vincent, Saint Vincent and the Grenadines",
        "gmt": "-04:00"
    },
    {
        "id": 333,
        "name": "Apia, Samoa",
        "gmt": "+14:00"
    },
    {
        "id": 334,
        "name": "San Marino, San Marino",
        "gmt": "+01:00"
    },
    {
        "id": 335,
        "name": "Sao Tome, Sao Tome and Principe",
        "gmt": "00:00"
    },
    {
        "id": 336,
        "name": "Riyadh, Saudi Arabia",
        "gmt": "+03:00"
    },
    {
        "id": 337,
        "name": "Dakar, Senegal",
        "gmt": "00:00"
    },
    {
        "id": 338,
        "name": "Belgrade, Serbia",
        "gmt": "+01:00"
    },
    {
        "id": 339,
        "name": "Mahe, Seychelles",
        "gmt": "+04:00"
    },
    {
        "id": 340,
        "name": "Freetown, Sierra Leone",
        "gmt": "00:00"
    },
    {
        "id": 341,
        "name": "Singapore, Singapore",
        "gmt": "+08:00"
    },
    {
        "id": 342,
        "name": "Lower Princes, Sint Maarten",
        "gmt": "-04:00"
    },
    {
        "id": 343,
        "name": "Bratislava, Slovakia",
        "gmt": "+01:00"
    },
    {
        "id": 344,
        "name": "Ljubljana, Slovenia",
        "gmt": "+01:00"
    },
    {
        "id": 345,
        "name": "Guadalcanal, Solomon Islands",
        "gmt": "+11:00"
    },
    {
        "id": 346,
        "name": "Mogadishu, Somalia",
        "gmt": "+03:00"
    },
    {
        "id": 347,
        "name": "Johannesburg, South Africa",
        "gmt": "+02:00"
    },
    {
        "id": 348,
        "name": "South Georgia, South Georgia and the South Sandwich Islands",
        "gmt": "-02:00"
    },
    {
        "id": 349,
        "name": "Seoul, South Korea",
        "gmt": "+09:00"
    },
    {
        "id": 350,
        "name": "Juba, South Sudan",
        "gmt": "+03:00"
    },
    {
        "id": 351,
        "name": "Ceuta, Spain",
        "gmt": "+01:00"
    },
    {
        "id": 352,
        "name": "Canary, Spain",
        "gmt": "00:00"
    },
    {
        "id": 353,
        "name": "Madrid, Spain",
        "gmt": "+01:00"
    },
    {
        "id": 354,
        "name": "Colombo, Sri Lanka",
        "gmt": "+05:30"
    },
    {
        "id": 355,
        "name": "Khartoum, Sudan",
        "gmt": "+02:00"
    },
    {
        "id": 356,
        "name": "Paramaribo, Suriname",
        "gmt": "-03:00"
    },
    {
        "id": 357,
        "name": "Longyearbyen, Svalbard and Jan Mayen",
        "gmt": "+01:00"
    },
    {
        "id": 358,
        "name": "Mbabane, Swaziland",
        "gmt": "+02:00"
    },
    {
        "id": 359,
        "name": "Stockholm, Sweden",
        "gmt": "+01:00"
    },
    {
        "id": 360,
        "name": "Zurich, Switzerland",
        "gmt": "+01:00"
    },
    {
        "id": 361,
        "name": "Damascus, Syria",
        "gmt": "+02:00"
    },
    {
        "id": 362,
        "name": "Taipei, Taiwan",
        "gmt": "+08:00"
    },
    {
        "id": 363,
        "name": "Dushanbe, Tajikistan",
        "gmt": "+05:00"
    },
    {
        "id": 364,
        "name": "Dar es Salaam, Tanzania",
        "gmt": "+03:00"
    },
    {
        "id": 365,
        "name": "Bangkok, Thailand",
        "gmt": "+07:00"
    },
    {
        "id": 366,
        "name": "Lome, Togo",
        "gmt": "00:00"
    },
    {
        "id": 367,
        "name": "Fakaofo, Tokelau",
        "gmt": "+13:00"
    },
    {
        "id": 368,
        "name": "Tongatapu, Tonga",
        "gmt": "+13:00"
    },
    {
        "id": 369,
        "name": "Port of Spain, Trinidad and Tobago",
        "gmt": "-04:00"
    },
    {
        "id": 370,
        "name": "Tunis, Tunisia",
        "gmt": "+01:00"
    },
    {
        "id": 371,
        "name": "Istanbul, Turkey",
        "gmt": "+03:00"
    },
    {
        "id": 372,
        "name": "Ashgabat, Turkmenistan",
        "gmt": "+05:00"
    },
    {
        "id": 373,
        "name": "Grand Turk, Turks and Caicos Islands",
        "gmt": "-05:00"
    },
    {
        "id": 374,
        "name": "Funafuti, Tuvalu",
        "gmt": "+12:00"
    },
    {
        "id": 375,
        "name": "St Thomas, U.S. Virgin Islands",
        "gmt": "-04:00"
    },
    {
        "id": 376,
        "name": "Kampala, Uganda",
        "gmt": "+03:00"
    },
    {
        "id": 377,
        "name": "Kiev, Ukraine",
        "gmt": "+02:00"
    },
    {
        "id": 378,
        "name": "Uzhgorod, Ukraine",
        "gmt": "+02:00"
    },
    {
        "id": 379,
        "name": "Zaporozhye, Ukraine",
        "gmt": "+02:00"
    },
    {
        "id": 380,
        "name": "Dubai, United Arab Emirates",
        "gmt": "+04:00"
    },
    {
        "id": 381,
        "name": "London, United Kingdom",
        "gmt": "00:00"
    },
    {
        "id": 382,
        "name": "Adak, United States",
        "gmt": "-10:00"
    },
    {
        "id": 383,
        "name": "Anchorage, United States",
        "gmt": "-09:00"
    },
    {
        "id": 384,
        "name": "Boise, United States",
        "gmt": "-07:00"
    },
    {
        "id": 385,
        "name": "Chicago, United States",
        "gmt": "-06:00"
    },
    {
        "id": 386,
        "name": "Denver, United States",
        "gmt": "-07:00"
    },
    {
        "id": 387,
        "name": "Detroit, United States",
        "gmt": "-05:00"
    },
    {
        "id": 388,
        "name": "Indianapolis, United States",
        "gmt": "-05:00"
    },
    {
        "id": 389,
        "name": "Knox, United States",
        "gmt": "-06:00"
    },
    {
        "id": 390,
        "name": "Marengo, United States",
        "gmt": "-05:00"
    },
    {
        "id": 391,
        "name": "Petersburg, United States",
        "gmt": "-05:00"
    },
    {
        "id": 392,
        "name": "Tell City, United States",
        "gmt": "-06:00"
    },
    {
        "id": 393,
        "name": "Vevay, United States",
        "gmt": "-05:00"
    },
    {
        "id": 394,
        "name": "Vincennes, United States",
        "gmt": "-05:00"
    },
    {
        "id": 395,
        "name": "Winamac, United States",
        "gmt": "-05:00"
    },
    {
        "id": 396,
        "name": "Juneau, United States",
        "gmt": "-09:00"
    },
    {
        "id": 397,
        "name": "Louisville, United States",
        "gmt": "-05:00"
    },
    {
        "id": 398,
        "name": "Monticello, United States",
        "gmt": "-05:00"
    },
    {
        "id": 399,
        "name": "Los Angeles, United States",
        "gmt": "-08:00"
    },
    {
        "id": 400,
        "name": "Menominee, United States",
        "gmt": "-06:00"
    },
    {
        "id": 401,
        "name": "Metlakatla, United States",
        "gmt": "-09:00"
    },
    {
        "id": 402,
        "name": "New York, United States",
        "gmt": "-05:00"
    },
    {
        "id": 403,
        "name": "Nome, United States",
        "gmt": "-09:00"
    },
    {
        "id": 404,
        "name": "Beulah, United States",
        "gmt": "-06:00"
    },
    {
        "id": 405,
        "name": "Center, United States",
        "gmt": "-06:00"
    },
    {
        "id": 406,
        "name": "New Salem, United States",
        "gmt": "-06:00"
    },
    {
        "id": 407,
        "name": "Phoenix, United States",
        "gmt": "-07:00"
    },
    {
        "id": 408,
        "name": "Sitka, United States",
        "gmt": "-09:00"
    },
    {
        "id": 409,
        "name": "Yakutat, United States",
        "gmt": "-09:00"
    },
    {
        "id": 410,
        "name": "Honolulu, United States",
        "gmt": "-10:00"
    },
    {
        "id": 411,
        "name": "Midway, United States Minor Outlying Islands",
        "gmt": "-11:00"
    },
    {
        "id": 412,
        "name": "Wake, United States Minor Outlying Islands",
        "gmt": "+12:00"
    },
    {
        "id": 413,
        "name": "Montevideo, Uruguay",
        "gmt": "-03:00"
    },
    {
        "id": 414,
        "name": "Samarkand, Uzbekistan",
        "gmt": "+05:00"
    },
    {
        "id": 415,
        "name": "Tashkent, Uzbekistan",
        "gmt": "+05:00"
    },
    {
        "id": 416,
        "name": "Efate, Vanuatu",
        "gmt": "+11:00"
    },
    {
        "id": 417,
        "name": "Vatican, Vatican",
        "gmt": "+01:00"
    },
    {
        "id": 418,
        "name": "Caracas, Venezuela",
        "gmt": "-04:00"
    },
    {
        "id": 419,
        "name": "Ho Chi Minh, Vietnam",
        "gmt": "+07:00"
    },
    {
        "id": 420,
        "name": "Wallis, Wallis and Futuna",
        "gmt": "+12:00"
    },
    {
        "id": 421,
        "name": "El Aaiun, Western Sahara",
        "gmt": "00:00"
    },
    {
        "id": 422,
        "name": "Aden, Yemen",
        "gmt": "+03:00"
    },
    {
        "id": 423,
        "name": "Lusaka, Zambia",
        "gmt": "+02:00"
    },
    {
        "id": 424,
        "name": "Harare, Zimbabwe",
        "gmt": "+02:00"
    }
]};

export const getStaticDefaultTimezones = () => {
	return [{
        "id": 120,
        "name": "Santiago, Chile",
        "gmt": "-03:00"
    }];
};