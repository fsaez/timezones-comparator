import React from 'react';
import renderer from 'react-test-renderer';
import TimezoneTable from './TimezoneTableContainer';
import { createStore } from 'redux';
import reducer from '../../reducers/reducer';
import { Provider } from 'react-redux';
import mockDateProvider from '../../tests/mockDateProvider';

describe('TimezoneTable tests', () => {

	beforeAll(() => {
		//since this component uses the current date, we need to mock it
  		mockDateProvider.mockDefaultDate();
	});

	test('it should render a table properly', () => {

		const store = createStore(reducer);

		const renderedComponent = renderer.create(
	    	<Provider store={ store }>
				<TimezoneTable />
			</Provider>
	  	);

	  	const componentInstance = renderedComponent.getInstance();
	  	let tree;
		
		tree = renderedComponent.toJSON();
		expect(tree).toMatchSnapshot();
	});

	afterAll(() => {
		//restore to original date
		mockDateProvider.resetDate();
	});
});