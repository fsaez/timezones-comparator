import React from 'react';
import { connect } from 'react-redux';
import TimezoneTable from './TimezoneTable';

class TimezoneTableContainer extends React.Component {
	render() {
		return <TimezoneTable timezones={ this.props.timezones } />;
	}
}

const mapStateToProps = (state) => ({
  timezones: state.timezones
});

export default connect(mapStateToProps)(TimezoneTableContainer);