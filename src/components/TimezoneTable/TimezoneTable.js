import React    from "react";
import template from "./TimezoneTable.jsx";
import PropTypes from "prop-types";

class TimezoneTable extends React.Component {
  render() {
    return template.call(this);
  }
}

TimezoneTable.propTypes = {
	timezones: PropTypes.array.isRequired
};

export default TimezoneTable;
