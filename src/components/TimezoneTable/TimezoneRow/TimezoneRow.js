import React    from "react";
import template from "./TimezoneRow.jsx";
import PropTypes from "prop-types";

class TimezoneRow extends React.Component {
  render() {
    return template.call(this);
  }
}

TimezoneRow.propTypes = {
	name: PropTypes.string.isRequired,
	gmt: PropTypes.string.isRequired
};

export default TimezoneRow;
