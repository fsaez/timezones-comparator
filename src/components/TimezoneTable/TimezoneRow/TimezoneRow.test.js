import React from 'react';
import renderer from 'react-test-renderer';
import TimezoneRow from './TimezoneRow';
import { createStore } from 'redux';
import mockDateProvider from '../../../tests/mockDateProvider';

describe('TimezoneRow tests', () => {
	
	beforeAll(() => {
		//since this component uses the current date, we need to mock it
		mockDateProvider.mockDefaultDate();
	});

	test('it should render a row properly', () => {

		const renderedComponent = renderer.create(
	    	<TimezoneRow name={ "Santiago, Chile" } gmt={ "-03:00" }/>
	  	);

	  	const componentInstance = renderedComponent.getInstance();
	  	let tree;

	  	console.log("test");
		
		tree = renderedComponent.toJSON();
		expect(tree).toMatchSnapshot();
	});

	afterAll(() => {
		//restore to original date
		mockDateProvider.resetDate();
	});
});