import "./TimezoneRow.css";
import React from "react";
import { getCurrentTimePlus } from '../../../services/TimeProvider';


function template() {
	let times = [];
	let timesCounter = 12;

	for (let i = 0; i < timesCounter; i++) {
		times[i] = getCurrentTimePlus(this.props.gmt, i);
		console.log("times[i]: " + times[i]);
	}

	return (
	<tr className="timezone-row">
    	<td className="timezone-row__item timezone-row__timzeone-name-item " key={ 0 }>{ this.props.name }</td>
    	{
    		times.map((time, index) => (<td className="timezone-row__item" key={ index }>{ time }</td>))
    	}
    </tr>);
};

export default template;
