import "./TimezoneTable.css";
import React from "react";
import TimezoneRow from './TimezoneRow';

function template() {
  return (
    <table className="timezone-table">
    	<tbody>
    		{ this.props.timezones.map(timezone => <TimezoneRow key={ timezone.id } name={ timezone.name } gmt={ timezone.gmt } />) }
    	</tbody>
    </table>
  );
};

export default template;
