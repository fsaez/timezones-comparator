import "./AddTimezoneField.css";
import React from "react";
import Typeahead from '../Typeahead';
import { getTimezones } from '../../services/TimezonesListProvider';

function template() {
  return (
  	<div className="add-timezone-field">
  		<Typeahead
  			placeholder={ "Start typing to add a timezone" }
  			data={ getTimezones() }
  			attributeName={ 'name' }
        onInputChange={ this.props.onInputChange }
  			onSelection={ (value) => { this.props.onTimezoneSelected(value); } }/>
  		<button onClick={ () => { this.props.onAddNewTimezone(this.props.value); } }>Add Timezone</button>
  	</div>

  );
};

export default template;
