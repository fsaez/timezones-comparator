import React    from "react";
import template from "./AddTimezoneField.jsx";
import PropTypes from "prop-types";

class AddTimezoneField extends React.Component {

  render () {
    return template.call(this);
  }
}

AddTimezoneField.propTypes = {
	value: PropTypes.object.isRequired,
	onTimezoneSelected: PropTypes.func.isRequired,
	onAddNewTimezone: PropTypes.func.isRequired
};

export default AddTimezoneField;
