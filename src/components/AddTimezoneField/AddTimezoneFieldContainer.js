import React from 'react';
import AddTimezoneField from './AddTimezoneField';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class AddTimezoneFieldContainer extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			value: {}
		};
	}

	onTimezoneSelected (value) {
		this.setState({ value: value });
	}

	onInputChange () {
		this.setState({ value: {} });
	}

	render() {
		return <AddTimezoneField
			value={ this.state.value }
			onTimezoneSelected={ this.onTimezoneSelected.bind(this) }
			onInputChange={ this.onInputChange.bind(this) }
			onAddNewTimezone={ this.props.onAddNewTimezone } />;
	}
}

const mapStateToProps = (state) => ({
	value: state.value
});

const mapDispatchToProps = (dispatch) => {
	const onAddNewTimezone = (value) => {
		return {
			type: 'ADD_TIMEZONE',
			name: value.name,
			gmt: value.gmt,
			id: value.id
		};
	};

	return bindActionCreators({ onAddNewTimezone: onAddNewTimezone }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTimezoneFieldContainer);

