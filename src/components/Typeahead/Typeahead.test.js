import React from 'react';
import renderer from 'react-test-renderer';
import Typeahead from './TypeaheadContainer';
import { getTimezones } from '../../services/TimezonesListProvider';

describe('it should render suggestions properly', () => {

	const renderedComponent = renderer.create(
    	<Typeahead
  			placeholder={ "Start typing to add a timezone" }
  			data={ getTimezones() }
  			attributeName={ 'name' }
  			onSelection={ (value) => { console.log('value: ', value); } }/>
  	);

  	const componentInstance = renderedComponent.getInstance();

  	let tree;

  	console.log(componentInstance.searchSuggestions);
	
	test('it should render no suggestions when nothing is typed', () => {
		tree = renderedComponent.toJSON();
		expect(tree).toMatchSnapshot();
	});

	test('it should render suggestions when \'S\' is typed', () => {
		componentInstance.searchSuggestions("S");
		tree = renderedComponent.toJSON();
		expect(tree).toMatchSnapshot();
	});

	test('it should render suggestions when \'N\' is typed', () => {

		componentInstance.searchSuggestions("N");
		tree = renderedComponent.toJSON();
		expect(tree).toMatchSnapshot();
	});

	test('it should render suggestions when \'London\' is typed', () => {

		componentInstance.searchSuggestions("London");
		tree = renderedComponent.toJSON();
		expect(tree).toMatchSnapshot();
	});

});


