import React from "react";
import PropTypes from 'prop-types';
import template from "./Typeahead.jsx";

class Typeahead extends React.Component {
	render() {
		return template.call(this);
	}
}

Typeahead.propTypes = {
	text: PropTypes.string.isRequired,
	value: PropTypes.object.isRequired,
	suggestions: PropTypes.array.isRequired,
	placeholder: PropTypes.string.isRequired,
	onFocusOut: PropTypes.func.isRequired,
	onFocusIn: PropTypes.func.isRequired,
	isListVisible: PropTypes.bool.isRequired,
	onSelection: PropTypes.func.isRequired,
	onInputChange: PropTypes.func,
	attributeName: PropTypes.string.isRequired
};

export default Typeahead;
