import "./Typeahead.css";
import React from "react";

function template() {
  return (
    <div className="typeahead">
      <input type="text"
      	value={ this.props.text }
      	placeholder={ this.props.placeholder }
      	onChange={ this.props.onInputChange }
      	onBlur={ this.props.onFocusOut }
      	onFocus={ this.props.onFocusIn }
      	className={ "typeahead__input" }>
      </input>
      <ul className={ "typeahead__list " + (this.props.isListVisible ? "" : "typeahead__list--hidden") }>
      	{ 
      		this.props.suggestions.map((suggestion, index) => (
      			<li 
      				className="typeahead__item"
      				onClick={ () => (this.props.onSelection(index)) } key={ index }>{ suggestion[this.props.attributeName] }
      			</li>
      		))
      	}
      </ul>
    </div>
  );
};

export default template;
