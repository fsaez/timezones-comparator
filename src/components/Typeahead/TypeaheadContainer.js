import React from 'react';
import Typeahead from './Typeahead';

class TypeaheadContainer extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			text: '',
			value: {},
			suggestions: [],
			visible: false,
			isListVisible: false
		};
	}

	onInputChange (e) {
		this.searchSuggestions(e.target.value);
	}

	onFocusOut () {
		//we set a timeout to allow the selected item to be clicked before hidding it
		setTimeout(() => {
			this.setState({ 
				isListVisible: false
			});
		}, 100);
	}

	onFocusIn () {
		this.setState({ 
			isListVisible: true
		});
	}

	onSelection (index) {
		this.setState({ 
			text: this.state.suggestions[index][this.props.attributeName],
			value: this.state.suggestions[index],
			isListVisible: false
		});
		this.props.onSelection(this.state.suggestions[index]);
	}

	searchSuggestions (textEntered) {
		const enteredText = textEntered.toLowerCase();
		let suggestions = [];
		let isListVisible = true;

		if (textEntered === '') {
			isListVisible = false;
		} else {
			suggestions = this.props.data.filter(item => (
				item[this.props.attributeName].toLowerCase().startsWith(enteredText) ||
				item[this.props.attributeName].toLowerCase().includes(" " + enteredText)
			));
		}
	
		this.setState({
			text: textEntered,
			value: {},
			suggestions: suggestions,
			isListVisible: isListVisible
		});
		
		if (this.props.onInputChange) {
			this.props.onInputChange();
		}
	}

	render () {
		return  <Typeahead 
					text={ this.state.text }
					value={ this.state.value }
					attributeName={ this.props.attributeName }
					placeholder={ this.props.placeholder }
					suggestions={ this.state.suggestions } 
					onInputChange={ this.onInputChange.bind(this) } 
					onSelection={ this.onSelection.bind(this) }
					isListVisible={ this.state.isListVisible } 
					onFocusOut={ this.onFocusOut.bind(this) }
					onFocusIn={ this.onFocusIn.bind(this) }/>;
	}
}

export default TypeaheadContainer;