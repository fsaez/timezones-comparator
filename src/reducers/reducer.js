import { getDefaultTimezones } from '../services/TimezonesListProvider';

const initialState = {
	value: {},
	timezones: getDefaultTimezones()
};

const hasTimezoneBeenAdded = (state, action) => (
  state.timezones.find(timezone => timezone.name === action.name)
);

const whatTimeIsItApp = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TIMEZONE':
      //if there is no action name it means that no object was passed so we won't add it to the state
    	if (hasTimezoneBeenAdded(state, action) || !action.name) {
    		return state;
    	} else {
    		return { ...state, timezones:  [ ...state.timezones, { name: action.name, gmt: action.gmt, id: action.id } ]};
    	}
    default:
      return state;
  }
};

export default whatTimeIsItApp;
