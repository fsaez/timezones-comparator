import React    from "react";
import template from "./TimezonesComparePage.jsx";

class TimezonesComparePage extends React.Component {
  render() {
    return template.call(this);
  }
}

export default TimezonesComparePage;
