import "./TimezonesComparePage.css";
import React from "react";
import TimezoneTable from '../../components/TimezoneTable';
import AddTimezoneField from '../../components/AddTimezoneField';

function template() {
  return (
    <div className="timezones-compare-page">
    	<AddTimezoneField />
    	<TimezoneTable />
    </div>
  );
};

export default template;
