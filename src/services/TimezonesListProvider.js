import { getStaticTimezones, getStaticDefaultTimezones } from '../config/timezones';

export const getTimezones = () => (
	getStaticTimezones()
);

export const getDefaultTimezones = () => (
	getStaticDefaultTimezones()
);