//Method that returns a HH:MM string based on a date object
export const formatDateToTime = (date) => {
	const formatedTime = ensure2digits(date.getHours()) + ':' + ensure2digits(date.getMinutes());
	return formatedTime;
};

//Method that returns a UTC date
export const getCurrentDate = () => {
	let now = new Date();
	let currentDateUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
	return currentDateUTC;
};

//Method that returns a time string (HH:MM format) based on a UTC and delta in hours
export const getCurrentTimePlus = (gmt, delta) => {
	let dateObject = getCurrentDate();
	dateObject.setHours(dateObject.getHours() + transformTimeToHours(gmt) + delta);
	return formatDateToTime(dateObject);
};

//Method that transforms a string of format HH:MM to the sum of those hours and minutes
export const transformTimeToHours = (time) => {
	const timeArray = time.toString().split(":");
	return Number(timeArray[0]) + Number(timeArray[1]) / 60;
};

//Method that returns a string of exactly 2 digits adding zeros to the left if number is just 1 digit
export const ensure2digits = (number) => {
	const numberWithZeros = "0" + number.toString();
    return numberWithZeros.slice(-2);
};