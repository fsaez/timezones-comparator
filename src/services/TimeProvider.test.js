import { transformTimeToHours, getCurrentTimePlus, formatDateToTime, ensure2digits } from './TimeProvider';
import mockDateProvider from '../tests/mockDateProvider';

describe("TimeProvider", () => {
	beforeEach(() => {
		mockDateProvider.mockDate("2017-06-13T18:00:20");
	});

	test("transformTimeToHours method", () => {
		expect(transformTimeToHours("-03:00")).toEqual(-3);
		expect(transformTimeToHours("0:30")).toEqual(0.5);
		expect(transformTimeToHours("10:30")).toEqual(10.5);
		expect(transformTimeToHours("1:00")).toEqual(1);
	});

	test("formatDateToTime method", () => {
		mockDateProvider.mockDate("2017-06-13T14:00:20");
		const date1 = new Date();
		expect(formatDateToTime(date1)).toEqual("14:00");

		mockDateProvider.mockDate("2017-06-13T01:00:20");
		const date2 = new Date()
		expect(formatDateToTime(date2)).toEqual("01:00");
	});

	test("ensure2digits method", () => {
		expect(ensure2digits("0")).toEqual("00");
		expect(ensure2digits("18")).toEqual("18");
	});

	test("getCurrentTimePlus method", () => {
		const currentTimePlusDelta = getCurrentTimePlus("-03:00", 0);
		expect(currentTimePlusDelta).toEqual("15:00");
	});

	afterEach(() => {
		mockDateProvider.resetDate();
	});
});