import React, { Component } from 'react';
import './App.css';
import TimezonesComparePage from './scenes/TimezonesComparePage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Welcome to timezones comparator</h2>
          <div>Here you can compare different timezones to see what time it is now and in the next hours</div>
        </div>
        <TimezonesComparePage />
      </div>
    );
  }
}

export default App;
