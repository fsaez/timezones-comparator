const DEFAULT_MOCK_DATE_STRING = "2017-06-13T18:41:20";

const mockDateProvider = (function () {

	let RealDate = global.Date;;

	const mockDate = (isoDate) => {
		global.Date = class extends RealDate {
			constructor () {
				return new RealDate(isoDate);
			}
		}
	};

	const mockDefaultDate = () => {
		mockDate(DEFAULT_MOCK_DATE_STRING);
	};

	const resetDate = () => {
		global.Date = RealDate;
	};

	return {
		mockDate: mockDate,
		mockDefaultDate: mockDefaultDate,
		resetDate: resetDate
	};
})();

export default mockDateProvider;